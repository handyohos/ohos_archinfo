# analyser服务部署说明



## 1. python服务自启动的systemd配置

analyser是一个Python服务，可使用systemd目录下的ohosarch.service配置文件把此服务配置为开机自动启动。

**安装方法**：把该文件拷贝到/usr/lib/systemd/system/目录（需要root权限），其中ExecStart以及User/Group信息需要根据用户实际情况进行修改。

**操作命令**：然后通过以下命令操作服务：

```shell
sudo systemctl enable ohosarch   # 使能ohosarch服务（开机后会自动启动）
sudo systemctl daemon-reload     # 修改ohosarch.service文件后，通过此命令重新加载配置

sudo systemctl start ohosarch    # 启动ohosarch服务
sudo systemctl stop ohosarch     # 停止ohosarch服务
sudo systemctl restart ohosarch  # 重新启动ohosarch服务

sudo systemctl status ohosarch   # 查看ohosarch服务的运行状态
```



## 2. symdb_analyser服务自身配置说明

symdb_analyser服务自身有一些配置信息，都在config/__init__.py文件中：

- SERVER_IP

  服务器的IP地址，可以直接配公网IP；如果通过nginx反向代理，则可以配置127.0.0.1。

- SERVER_PORT

  服务的端口号，根据需要配置。

- HTML_PATH

  服务的HTML相关静态网页路径。

- PRODUCT_ROOT_PATH

  各个产品的数据库相关文件根路径。其目录结构组织如下：

  ```shell
  ├── db
  │   └── ohos_rk3568				# 产品名称
  │       └── 3.2.7.5				# 产品版本
  │           └── archinfo.db		# 产品架构信息数据库
  └── graphviz					# 产品的架构信息图
      └── ohos_rk3568				# 产品名称
          └── 3.2.7.5				# 产品版本
              ├── components		# 产品的部件相关架构信息图
              │   └── window_manager	# 部件名称
              │       ├── window_manager-deps.dot		# 部件的graphviz信息原始文件
              │       ├── window_manager-deps.png		# 部件的graphviz信息png文件
              │       └── window_manager-deps.svg		# 部件的graphviz信息svg文件
              └── modules			# 产品的模块相关架构信息图
                  └── libbluetooth_hci_proxy_1.0.z.so	# 模块名称
                      ├── libbluetooth_hci_proxy_1.0.z.so-deps.dot	# 模块的graphviz信息原始文件
                      ├── libbluetooth_hci_proxy_1.0.z.so-deps.png	# 模块的graphviz信息png文件
                      └── libbluetooth_hci_proxy_1.0.z.so-deps.svg	# 模块的graphviz信息svg文件
  ```

- GRAPHVIZ_ENABLE_CACHE

  是否缓存生成的GraphViz图，可大大提高性能；默认使能，调试时可关闭。

- REST_PREFIX

  架构信息rest服务的前缀，不要修改。nginx反向代理配置时可使用此目录。



## 3. nginx服务器代理配置

nginx服务器配置只需要修改/etc/nginx/sites-enabled/default中的以下配置：

```groovy
server {
    root /home/z00325844/demo/archinfo/deploy/OHOS_Arch_Info/analyser/html;	# 配置静态页面的地址，注意此目录需要设置755，确保nginx服务能访问，与symdb_analyser配置的HTML_PATH相同。
    
    location /symdb/ {		# 配置/symdb REST服务的反向代理，与analyser配置的REST_PREFIX相同
        proxy_pass http://localhost:9999/symdb/;	# analyser的内部地址
    }
    
    ...
}
```

