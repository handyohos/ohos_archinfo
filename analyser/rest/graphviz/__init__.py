#! /usr/bin/env python
#coding=utf-8

from .graphviz_builder import GraphVizBuilder

def requestingGraphviz(xargs):
	if xargs and "format" in xargs and xargs["format"] in ("svg", "png", "dot"):
		return True
	return False
