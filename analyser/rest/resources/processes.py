#! /usr/bin/env python
#coding=utf-8

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../"))

from utils import ResterHelper
from utils import ResterStringBuilder

VMA_SIZE_KEYS = ("Size", "Rss", "Pss", "Swap", "SwapPss", "Shared_Clean", "Shared_Dirty", "Private_Clean", "Private_Dirty", "Referenced", "Anonymous", "KernelPageSize", "MMUPageSize", "LazyFree", "AnonHugePages", "ShmemPmdMapped", "Shared_Hugetlb", "Private_Hugetlb", "Locked", "THPeligible", "FilePmdMapped" )

VMA_ADDR_KEYS = ("start", "end", "perm", "offset", "dev", "idx")

class ProcessResults(ResterStringBuilder):
	def __init__(self, keys, row):
		for idx, key in enumerate(keys):
			self[key] = row[idx]

	def getStr(self, xargs=None, keys=None):
		if xargs and "details" in xargs:
			ordered_keys = ("process", "pid", "ppid", "category", "threads", "objects", "name", "processes", "vmas") + VMA_SIZE_KEYS
		else:
			ordered_keys = ("pid", "ppid", "name", "category", "threads", "objects", "vmas") + VMA_SIZE_KEYS
		return ResterStringBuilder.getStr(self, xargs, ordered_keys)

class ProcessObjectResults(ResterStringBuilder):
	def __init__(self, keys, row):
		for idx, key in enumerate(keys):
			self[key] = row[idx]

	def getStr(self, xargs=None, keys=None):
		ordered_keys = ("id", "process", "name", "vmas") + VMA_SIZE_KEYS
		return ResterStringBuilder.getStr(self, xargs, ordered_keys)

class ProcessThreadResults(ResterStringBuilder):
	def __init__(self, keys, row):
		for idx, key in enumerate(keys):
			self[key] = row[idx]

	def getStr(self, xargs=None, keys=None):
		ordered_keys = ("tid", "thread", "pid", "process")
		return ResterStringBuilder.getStr(self, xargs, ordered_keys)

class ProcessVmaResults(ResterStringBuilder):
	def __init__(self, keys, row):
		for idx, key in enumerate(keys):
			self[key] = row[idx]

	def getStr(self, xargs=None, keys=None):
		ordered_keys = ("id", "process", "name") + VMA_ADDR_KEYS + VMA_SIZE_KEYS
		return ResterStringBuilder.getStr(self, xargs, ordered_keys)

class ProcessesRestMgr(object):
	def __init__(self, product):
		self._product = product

	def get_all(self, xargs=None):
		if xargs and "details" in xargs:
			keys = ("process", "pid", "ppid", "category", "threads", "objects", "name", "processes", "vmas") + VMA_SIZE_KEYS
			sqlcmd = "select %s from smaps_details" % ", ".join(keys)
		else:
			keys = ("pid", "ppid", "name", "category", "threads", "objects", "vmas") + VMA_SIZE_KEYS
			sqlcmd = "select %s from processes" % ", ".join(keys)
		cursor = self._product.getCursor()
		cursor.execute(sqlcmd)

		processes = []
		for row in cursor:
			processes.append(ProcessResults(keys, row))
		return processes

	def doRestRequest(self, id, mod, args):
		mCmdMap = {
			'objects':self._dbGetProcessObjects,
			'threads':self._dbGetProcessThreads,
			'vmas':self._dbGetProcessVmas,
		}
		vals = mCmdMap[mod](id, args)
		return ResterHelper.build_array_content(vals, args)

	def _dbGetProcessObjects(self, id, args):
		keys = ("id", "process", "name", "vmas") + VMA_SIZE_KEYS
		sqlcmd = "select %s from smaps_details where pid=%d" % (", ".join(keys), int(id))
		cursor = self._product.getCursor()
		cursor.execute(sqlcmd)

		objects = []
		for row in cursor:
			objects.append(ProcessObjectResults(keys, row))
		return objects

	def _dbGetProcessThreads(self, id, args):
		keys = ("tid", "thread", "pid", "process")
		sqlcmd = "select %s from threads_details where pid=%d" % (", ".join(keys), int(id))
		cursor = self._product.getCursor()
		cursor.execute(sqlcmd)

		objects = []
		for row in cursor:
			objects.append(ProcessThreadResults(keys, row))
		return objects

	def _dbGetProcessVmas(self, id, args):
		keys = ("id", "process", "name") + VMA_ADDR_KEYS + VMA_SIZE_KEYS
		if args["type"] == "object":
			sqlcmd = "select %s from vmas_details where smap_id=%d" % (", ".join(keys), int(id))
		else:
			sqlcmd = "select %s from vmas_details where pid=%d" % (", ".join(keys), int(id))
		cursor = self._product.getCursor()
		cursor.execute(sqlcmd)

		vmas = []
		for row in cursor:
			vmas.append(ProcessVmaResults(keys, row))
		return vmas

if __name__ == "__main__":
	import products

	product_mgr = products.Products()
	prod = product_mgr.get_product_by_name("openharmony", "rk3568")
	prod.load_db()

	mgr = ProcessesRestMgr(prod)

	#print(mgr._dbGetProcessObjects(1, {}))
	#print(mgr._dbGetProcessVmas(1, {}))

	print(mgr.doRestRequest(1, "objects", {}))

