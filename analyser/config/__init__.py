#! /usr/bin/env python
#coding=utf-8

SERVER_IP="127.0.0.1"
#SERVER_IP="7.190.75.123"
SERVER_PORT=9999

# static html files path
HTML_PATH="/home/z00325844/demo/archinfo/deploy/OHOS_Arch_Info/analyser/html"


# Product root path include:
#   db: product architecture information db organized with product_name/product_version/symdb.db
#   graphviz: generating graphviz related files for caching
PRODUCT_ROOT_PATH="/home/z00325844/demo/archinfo/deploy/dynamic"

GRAPHVIZ_ENABLE_CACHE=True


REST_PREFIX="symdb"
