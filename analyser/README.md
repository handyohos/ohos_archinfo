# OpenHarmony实时架构信息analyser模块说明

analyser模块用于对[collector](../collector/README.md)收集到的结构化架构信息数据进行分析并展示，其总体架构如下：

![image-20221107213530061](img/analyser.png)

​	其中REST服务提供数据库的分析接口，静态页面服务通过JS调用REST服务接口，并通过展示页面呈现。



## 1. 使用方法

​	analyser是基于Python的一个WEB服务，使用前需要先完成该服务的配置：

### 1.1 配置说明

​	analyser服务需要配置的内容在[config/__init__.py](config/__init__.py)文件中：

| 名称                  | 说明                                                         |
| --------------------- | ------------------------------------------------------------ |
| SERVER_IP             | 服务器的IP地址                                               |
| SERVER_PORT           | 服务器的端口                                                 |
| HTML_PATH             | analyser/html的绝对路径，使用绝对路径可以保证无论在哪里启动analyser服务都可正常访问静态页面资源。 |
| PRODUCT_ROOT_PATH     | 产品的架构信息数据根目录：<br/>db目录：使用者需要在此目录下按照product_name/product_version/archinfo.db的方式归档架构信息数据库。<br/>graphviz：运行时analyser会自动创建此目录，用于缓存通过graphviz生成的图片资源。 |
| GRAPHVIZ_ENABLE_CACHE | 是否使能graphviz的cache功能；生产环境建议默认使能，否则会每次请求都执行graphviz来生成图片。 |

### 1.2 启动方法

​	analyser启动方法为：python2 analyser.py

> 依赖说明：
>
> analyser使用过程中依赖graphviz生成依赖关系图，需要确保服务器已安装。
>
> ​	Ubuntu服务器的安装方法参考：sudo apt install graphviz

### 1.3 部署方式

​	analyser是一个简易的WEB服务，为了支持高性能多用户访问，可以把analyser部署到nginx服务器上，通过nginx做反向代理，具体方式参考[架构信息服务器部署说明](deploy/README.md)。



## 2. TOP依赖规则

![image-20221108091708507](img/rules.png)

| 规则名                | 类别     |                                                              | 看护方式                               |
| --------------------- | -------- | ------------------------------------------------------------ | -------------------------------------- |
| NO-Depends-On-SA      | 强制规则 | 【接口与实现分离】SA模块都是实现模块，不允许被依赖。         | 编译后通过门禁来看护非法依赖。         |
| NO-Depends-ON-NAPI    | 强制规则 | 【分层原则】NAPI模块是给应用的接口模块，不允许系统模块直接依赖。 | 整改后通过LD_LIBRARY_PATH来禁止依赖。  |
| Depends-InnerAPI-Only | 强制规则 | 【部件化基本要求】部件仅能依赖其它部件的Inner API模块。      | 编译后通过门禁来看护增量的跨部件依赖。 |
| No-Duplicate-Symbols  | 强制规则 | 【基本要求】系统中不同模块不能提供重名的对外公开符号。       | 通过架构信息系统看护重复符号。         |
| InnerAPI-No-Static    | 强制规则 | 【基本要求】部件间依赖不允许使用静态库，特殊情况白名单审批。 |                                        |
| Chipset SDK           | 强制规则 | 芯片组件只能依赖系统组件Chipset SDK中的模块，Chipset SDK模块需白名单评审。 |                                        |
| Platform SDK          | 强制规则 | Public API模块只能系统系统组件Platform SDK中的模块，Platform SDK模块需白名单评审。 |                                        |



## 3. 代码结构说明

- [html](html/README.md)：提供静态页面，主要包括HTML、JavaScript和CSS等页面资源。
- [loader](loader/README.md)：用于加载各个产品的架构信息数据库。
- [rest](rest/README.md)：基于已加载的架构信息数据库提供REST接口供静态页面展示相应内容。