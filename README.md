# OpenHarmony实时架构信息收集与分析系统



本项目通过对OpenHarmony的编译产物进行分析，收集相应的架构信息，形成结构化数据库，并提供分析与展示平台，可视化展示架构信息。

![image-20221107200739990](img/overview.png)

​	相关架构信息包括：模块信息，部件信息，依赖关系树，内存信息等。关键的架构信息定义如下：

## 1. 关键架构信息说明

### 1.1 模块的依赖信息

![image-20221107191614222](img/mod-deps.png)

| **名称**       | **英文**          | **统计值** | **含义**                            |
| -------------- | ----------------- | ---------- | ----------------------------------- |
| 直接依赖       | **deps**          | **D(n)**   | 直接依赖的模块                      |
| 跨部件直接依赖 | **deps_external** | **D_e(n)** | 直接依赖其它部件的模块              |
| 部件内直接依赖 | **deps_internal** | **D_n(n)** | 直接依赖部件内的模块                |
| 间接依赖       | **deps_indirect** | **D_I(n)** | 间接依赖的模块                      |
| 依赖总数       | **deps_total**    | **D_T(n**) | D(n)+D_I(n)直接和间接依赖的模块汇总 |
| 依赖深度       | **deps_depth**    | **D_D**    | 模块的依赖深度                      |

### 1.2 模块的被依赖信息

![image-20221107191726226](img/mod-dependedBy.png)

| **名称**         | **英文**                | **统计值** | **含义**                            |
| ---------------- | ----------------------- | ---------- | ----------------------------------- |
| 被直接依赖       | **dependedBy**          | **B(n)**   | 被直接依赖的模块                    |
| 跨部件直接被依赖 | **dependedBy_external** | **B_e(n)** | 被其它部件直接依赖的模块            |
| 部件内直接被依赖 | **dependedBy_internal** | **B_n(n)** | 被本部件内直接依赖的模块            |
| 被间接依赖       | **dependedBy_indirect** | **B_I(n)** | 被间接依赖的模块                    |
| 被依赖总数       | **dependedBy_total**    | **B_T(n)** | B(n)+B_I(n)直接和间接依赖的模块汇总 |
| 被依赖深度       | **dependedBy_depth**    | **B_D**    | 模块的被依赖深度                    |

### 1.3 模块的符号信息

![image-20221107191924686](img/symbols.png)

| **名称**         | **英文**                | **统计值**  | **含义**                                                     |
| ---------------- | ----------------------- | ----------- | ------------------------------------------------------------ |
| 公开符号         | **provided**            | **S_P(n)**  | 模块提供的公开符号                                           |
| 真公开符号       | **truly** **provided**  | **S_Pt(n)** | 模块提供的被使用的公共符号（used）                           |
| 伪公开符号       | **falsely provided**    | **S_Pf(n)** | 模块提供的未被使用的公共符号（unused）                       |
| 未定义符号       | **undefined**           | **S_U(n)**  | 模块未定义的符号                                             |
| 已匹配未定义符号 | **matched undefined**   | **S_Ut(n)** | 模块中未定义符号与被直接依赖模块提供的公开符号的匹配符号     |
| 未匹配未定义符号 | **unmatched undefined** | **S_Uf(n)** | 模块中未定义符号与被直接依赖模块中提供的公开符号无法匹配的符号 |
| 重复符号         | **duplicates**          | **S_Ud(n)** | 在deps依赖中匹配到的重复符号个数                             |
| 调用符号         | **calls**               | **calls**   | 模块A调用模块B的符号                                         |

### 1.4 模块的分类

如下图所示，系统中的模块详细分类如下：

![image-20221107193655872](img/deps-details.png)

其中Inner API分为两大类：

- 跨组件Inner API：

  跨组件Inner API是指被芯片组件模块依赖或者被应用进程加载的NAPI依赖的模块。

- 跨部件Inner API：

  跨部件Inner API表示只在系统组件内部件之间的依赖模块。

综合以上分类，模块间的依赖模型如下：

![image-20221110211442426](analyser/html/images/groups.png)

### 1.5 跨组件Inner API分类

如下图所示，跨组件的Inner API分为两类：

- Platform SDK

  应用加载的NAPI模块所依赖的系统组件模块。

- Chipset SDK

  芯片组件模块依赖的系统组件模块。

![image-20221107193556734](img/sdks.png)



## 2. 详细模块说明

[collector模块](collector/README.md)

[analyser模块](analyser/README.md)



上库命令：

```sh
git push https://gitee.com/handyohos/ohos_archinfo HEAD:refs/heads/master
```


