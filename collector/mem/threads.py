#! /usr/bin/env python
#coding=utf-8

import os
import codecs

def parse_threads_file(filename):
	threads = []
	with codecs.open(filename, 'r', 'utf-8') as f:
		tidIdx = -1
		for line in f.readlines():
			line = line.strip()
			if "shell ps " in line or "No such file or directory" in line or "Not support" in line:
				continue
			parts = line.split()
			if len(parts) < 5:
				continue

			if tidIdx < 0:
				tidIdx = parts.index("TID")
				continue

			# Skip header line
			threads.append({"tid": int(parts[tidIdx]), "name": parts[-1].strip()})

	return threads

if __name__ == "__main__":
	print(parse_threads_file("/home/handy/Documents/code/demo/archinfo/rk3568_img/packages/phone/mem/pids/threads/1_threads.txt"))
