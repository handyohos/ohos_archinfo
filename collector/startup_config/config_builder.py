#! /usr/bin/env python
#coding=utf-8

import string
import sys

sys.path.append('../')
from tablebuilder import TableBuilder
class FileTableBuilder(TableBuilder):
    def __init__(self, cursor):
        TableBuilder.__init__(self, cursor, "config_file")

    def getColumns(self):
        return ("fileId INTEGER PRIMARY KEY", "fileName STRING NOT NULL", "subsystemName STRING", "partName STRING")

    def getKeys(self):
        return ("fileId", "fileName", "subsystemName", "partName")

class CmdTableBuilder(TableBuilder):
    def __init__(self, cursor):
        TableBuilder.__init__(self, cursor, "config_cmd")

    def getColumns(self):
        return ("name STRING NOT NULL", "jobId INTEGER DEFAULT(-1)", "content STRING", "fileId INTEGER")

    def getColumnsExtraInfo(self):
        return ("id INTEGER PRIMARY KEY", "FOREIGN KEY(jobId) REFERENCES config_job(jobId)")

    def getKeys(self):
        return ("name", "jobId", "content", "fileId")

class JobTableBuilder(TableBuilder):
    def __init__(self, cursor):
        TableBuilder.__init__(self, cursor, "config_job")

    def getColumns(self):
        return ("name STRING PRIMARY KEY", "jobId INTEGER", "condition STRING",
            "serviceId INTEGER", "fileId INTEGER", "jobPriority INTEGER", "executionTime REAL")

    def getColumnsExtraInfo(self):
        return ("FOREIGN KEY(serviceId) REFERENCES config_service(serviceId)",
            "FOREIGN KEY(fileId) REFERENCES config_file(fileId)")

    def getKeys(self):
        return ("name", "jobId", "condition", "serviceId", "fileId", "jobPriority", "executionTime")

class ServiceSocketTableBuilder(TableBuilder):
    def __init__(self, cursor):
        TableBuilder.__init__(self, cursor, "config_service_socket")

    def getColumns(self):
        return ("name STRING PRIMARY KEY", "serviceId INTEGER", "family STRING", "type STRING",
            "protocol STRING", "permissions STRING", "uid STRING", "gid STRING", "option STRING")

    def getColumnsExtraInfo(self):
        return ("FOREIGN KEY(serviceId) REFERENCES config_service(serviceId)",
            "FOREIGN KEY(serviceId) REFERENCES config_service(serviceId)")

    def getKeys(self):
        return ("name", "serviceId", "family", "type", "protocol", "permissions", "uid", "gid", "option")

class ServiceFileTableBuilder(TableBuilder):
    def __init__(self, cursor):
        TableBuilder.__init__(self, cursor, "config_service_file")

    def getColumns(self):
        return ("name STRING NOT NULL", "serviceId INTEGER", "content STRING")

    def getColumnsExtraInfo(self):
        return ("id INTEGER PRIMARY KEY", "FOREIGN KEY(serviceId) REFERENCES config_service(serviceId)")

    def getKeys(self):
        return ("name", "serviceId", "content")

class ServiceTableBuilder(TableBuilder):
    def __init__(self, cursor):
        TableBuilder.__init__(self, cursor, "config_service")

    def getKeys(self):
        return ("name", "serviceId", "path", "uid", "gid", "once", "console", "notify_state", "on_demand", "sandbox",
            "disabled", "critical_enable", "limit_time", "limit_count", "importance", "caps", "cpu_core", "write_pid",
            "d_caps", "permission", "permission_acls", "apl", "secon",
            "start_mode", "boot_job", "start_job", "stop_job", "restart_job", "fileId")

    def getColumnsExtraInfo(self):
        return ()

    def getColumns(self):
        return ("name STRING PRIMARY KEY", "serviceId INTEGER", "path STRING", "uid STRING", "gid STRING",
            "once bool", "console bool", "notify_state bool", "on_demand bool", "sandbox bool", "disabled bool",
            "critical_enable bool", "limit_time INTEGER", "limit_count INTEGER", "importance INTEGER",
            "caps STRING", "cpu_core STRING", "write_pid STRING",
            "d_caps STRING", "permission STRING", "permission_acls STRING", "apl STRING", "secon STRING",
            "start_mode STRING", "boot_job STRING", "start_job STRING", "stop_job STRING", "restart_job STRING", "fileId INTEGER")

